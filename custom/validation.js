function inThongBaoLenForm(element, tinnhan) {
  var tbTKNVEl = document.querySelector(element);

  tbTKNVEl.innerHTML = tinnhan;

  // nếu có tin nhắn thì displablock
  if (tinnhan) {
    tbTKNVEl.style.display = "inline-block";
  }

  // nếu tin nhắn là rỗng ("") thì display none
  if (tinnhan === "") {
    tbTKNVEl.style.display = "none";
  }
}

function kiemTraTrong(value, element) {
  // console.log(value, element);
  if (value === "" || value === 0) {
    inThongBaoLenForm(element, "không được để trống");
    return false;
  } else {
    inThongBaoLenForm(element, "");
    return true;
  }
}

function kiemTraTaiKhoan(value, id) {
  if (value.match(/^[0-9]{4,6}$/gi)) {
    inThongBaoLenForm(id, "");
    return true;
  } else {
    inThongBaoLenForm(id, "Tài khoản phải từ 4-6 ký số ");
    return false;
  }
}

function kiemTraHoVaTen(value, element, tinnhan) {
  if (value.match(/[0-9]/gi)) {
    inThongBaoLenForm(element, tinnhan);
    return false;
  } else {
    inThongBaoLenForm(element, "");
    return true;
  }
}

function kiemTraEmail(value, element, tinnhan) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (value.match(re)) {
    inThongBaoLenForm(element, "");
    return true;
  } else {
    inThongBaoLenForm(element, tinnhan);
    return false;
  }
}

function kiemTraDinhDangNgayThang(value, element, tinnhan) {
  const re = /(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/((19|20)\d\d)/;

  if (value.match(re)) {
    inThongBaoLenForm(element, "");
    return true;
  } else {
    inThongBaoLenForm(element, tinnhan);
    return false;
  }
}

function kiemTraLuong(value, element, tinnhan) {
  if (value >= 1000000 && value <= 20000000) {
    inThongBaoLenForm(element, "");
    return true;
  } else {
    inThongBaoLenForm(element, tinnhan);
    return false;
  }
}

function kiemTraChucVu(value, element, tinnhan) {
  if (value === "Sếp" || value === "Trưởng phòng" || value === "Nhân viên") {
    inThongBaoLenForm(element, "");
    return true;
  } else {
    inThongBaoLenForm(element, tinnhan);
    return false;
  }
}

function kiemTraGioLam(value, element, tinnhan) {
  if (value >= 80 && value <= 200) {
    inThongBaoLenForm(element, "");
    return true;
  } else {
    inThongBaoLenForm(element, tinnhan);
    return false;
  }
}

function validation(objValueofElement) {
  console.log("in cái này nha", objValueofElement.ngayThang);

  //kiểm tra tài khoản nhân viên
  // START==========================================================
  var isKiemTraTrongTknv = kiemTraTrong(
    objValueofElement.taiKhoanNhanVien,
    "#tbTKNV"
  );
  // nếu người dùng không để trống thì kiểm tra tiếp
  // nếu người dùng để trống thì dừng kiểm trả function tiếp theo
  if (isKiemTraTrongTknv) {
    // nếu isKiemTraTrongTknv nghĩa là người dùng không để trống thì kiểm tra tiếp điều kiện tiếp theo
    var isKiemTraTaiKhoan = kiemTraTaiKhoan(
      objValueofElement.taiKhoanNhanVien,
      "#tbTKNV"
    );
  }
  // END==========================================================

  //kiểm tra tên
  // START==========================================================
  var isKiemTraTrongTen = kiemTraTrong(objValueofElement.ten, "#tbTen");
  if (isKiemTraTrongTen) {
    var isKiemTraTen = kiemTraHoVaTen(
      objValueofElement.ten,
      "#tbTen",
      "Tên nhân viên phải là chữ, không để trống"
    );
  }
  // END==========================================================

  //kiểm tra email
  // START==========================================================
  var isKiemTraTrongEmail = kiemTraTrong(objValueofElement.email, "#tbEmail");
  if (isKiemTraTrongEmail) {
    var isKiemTraEmail = kiemTraEmail(
      objValueofElement.email,
      "#tbEmail",
      "Email phải đúng định dạng"
    );
  }
  // END==========================================================

  //kiểm tra ngày tháng
  // START==========================================================
  var isKiemTraTrongNgayThang = kiemTraTrong(
    objValueofElement.ngayThang,
    "#tbNgay"
  );
  if (isKiemTraTrongNgayThang) {
    var isKiemTraNgayThang = kiemTraDinhDangNgayThang(
      objValueofElement.ngayThang,
      "#tbNgay",
      "Định dạng mm/dd/yyyy"
    );
  }
  // END==========================================================

  //kiểm tra Email
  // START==========================================================
  var isKiemTraTrongMatKhau = kiemTraTrong(
    objValueofElement.matKhau,
    "#tbMatKhau"
  );
  // END==========================================================

  //kiểm tra Chức Vụ
  var isKiemTraChucVu = kiemTraChucVu(
    objValueofElement.chucVu,
    "#tbChucVu",
    "Chọn Chức vụ"
  );

  //kiểm tra lương căn bản
  // START==========================================================
  var isKiemTraTrongLuongCoBan = kiemTraTrong(
    objValueofElement.luongCoBan,
    "#tbLuongCB"
  );
  if (isKiemTraTrongLuongCoBan) {
    var isKiemTraLuongCoBan = kiemTraLuong(
      objValueofElement.luongCoBan,
      "#tbLuongCB",
      "Lương cơ bản 1 000 000 - 20 000 000"
    );
  }
  // END==========================================================

  //kiểm tra giờ làm
  // START==========================================================
  var isKiemTraTrongGioLam = kiemTraTrong(
    objValueofElement.gioLam,
    "#tbGiolam"
  );
  if (isKiemTraTrongGioLam) {
    var isKiemTraGioLam = kiemTraGioLam(
      objValueofElement.gioLam,
      "#tbGiolam",
      "Nhập số giờ làm !!!"
    );
  }
  // END==========================================================

  // Tất cả đều đúng thì trả về true, nếu sai thì trả về false
  // START==========================================================
  if (
    isKiemTraTrongTknv &&
    isKiemTraTaiKhoan &&
    isKiemTraTrongTen &&
    isKiemTraTen &&
    isKiemTraTrongEmail &&
    isKiemTraEmail &&
    isKiemTraTrongNgayThang &&
    isKiemTraNgayThang &&
    isKiemTraTrongMatKhau &&
    isKiemTraChucVu &&
    isKiemTraTrongLuongCoBan &&
    isKiemTraLuongCoBan &&
    isKiemTraTrongGioLam &&
    isKiemTraGioLam
  ) {
    return true;
  } else {
    return false;
  }
}
// END==========================================================
