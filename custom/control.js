var arrayEmployee = [];

function Employee(
  _taiKhoanNhanVien,
  _ten,
  _email,
  _matKhau,
  _ngayThang,
  _luongCoBan,
  _chucVu,
  _gioLam
) {
  this.taiKhoanNhanVien = _taiKhoanNhanVien;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayThang = _ngayThang;
  this.luongCoBan = _luongCoBan;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tinhLuongTheoChucVu = function () {
    var luongCanBan = 0;
    if (this.chucVu === "Sếp") {
      luongCanBan = this.luongCoBan * 3;
    } else if (this.chucVu === "Trưởng phòng") {
      luongCanBan = this.luongCoBan * 2;
    } else if (this.chucVu === "Nhân viên") {
      luongCanBan = this.luongCoBan * 1;
    }
    return luongCanBan;
  };
  this.tinhTongLuong = function () {
    var tongLuong = 0;
    tongLuong = this.tinhLuongTheoChucVu() * this.gioLam;
    return tongLuong;
  };
  this.xepLoai = function () {
    var xepLoai = "";
    if (this.gioLam >= 192) {
      xepLoai = "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      xepLoai = "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      xepLoai = "Nhân viên khá";
    } else {
      xepLoai = "Nhân viên lười biếng";
    }
    return xepLoai;
  };
}

//Lưu giá trị LOCAL STORAGE
function luuStorage(value) {
  //b1: tạo 1 biến js để lấy giá trị bên ngoài để chuyển đổi thành JSON trước khi lưu
  var jsonn = JSON.stringify(value);
  //b2: lưu biến đó lên local storage
  localStorage.setItem("LOCAL_STRING", jsonn);
}

//Lấy giá trị từ Local  storage
function layStorage(key) {
  //b1: lấy "string của JSON" bằng cách tạo 1 biến của js và lưu string vào
  var myArrStr = localStorage.getItem(key);
  //b2: Chuyển biến vừa tạo thành Js.
  var myArr = JSON.parse(myArrStr);
  //b3: return về biến vừa tạo;
  return myArr;
}

function render(arrayEmployeeValue) {
  var render = "";

  arrayEmployeeValue.forEach(function (item) {
    render += `<tr>
                <td>${item.taiKhoanNhanVien}</td> 
                <td>${item.ten}</td>
                <td>${item.email}</td>
                <td>${item.ngayThang}</td>
                <td>${item.chucVu}</td>
                <td>${item.tinhTongLuong()}</td> 
                <td>${item.xepLoai()}</td> 

                <td class="d-inline-flex">
	                  <button class="btn btn-dark mr-1" data-toggle="modal"
                    data-target="#myModal" onclick="sua(${
                      item.taiKhoanNhanVien
                    })" >Sửa</button>
	                  <button class="btn btn-danger" onclick="xoa(${
                      item.taiKhoanNhanVien
                    })" >Xóa</button>
                </td>
               </tr>`;
  });
  document.getElementById("tableDanhSach").innerHTML = render;
}

function xoa(itemTaiKhoanNhanVien) {
  //lặp qua mảng để tìm index đang có trong kho
  var index = arrayEmployee.findIndex(function (item) {
    const result = item.taiKhoanNhanVien === itemTaiKhoanNhanVien;
    return result;
  });

  // xóa 1 phần tử trong kho bằng index
  arrayEmployee.splice(index, 1);

  // render lại kho sau khi xóa
  render(arrayEmployee);

  //lưu kho vào localStoreage
  // để khi reload không hiển thị lại cái vừa xóa
  luuStorage(arrayEmployee);
}
///  validation

//DOM
function layGiaTriTuForm(param) {
  var taiKhoanNhanVien = document.querySelector("#tknv").value;
  var ten = document.querySelector("#name").value;
  var email = document.querySelector("#email").value;
  var matKhau = document.querySelector("#password").value;
  var ngayThang = document.querySelector("#datepicker").value;
  var luongCoBan = document.querySelector("#luongCB").value * 1;
  var chucVu = document.querySelector("#chucvu").value;
  var gioLam = document.querySelector("#gioLam").value * 1;

  return {
    taiKhoanNhanVien,
    ten,
    email,
    matKhau,
    ngayThang,
    luongCoBan,
    chucVu,
    gioLam,
  };
}

function add(obj) {
  var newEmployee = new Employee(
    obj.taiKhoanNhanVien * 1,
    obj.ten,
    obj.email,
    obj.matKhau,
    obj.ngayThang,
    obj.luongCoBan,
    obj.chucVu,
    obj.gioLam
  );
  arrayEmployee.push(newEmployee);
}

// hàm này dùng khi trang web mới chạy hoặc f5
function initLocalStorage() {
  // lưu Arr ở local storage
  var arrfromLocalStorage = layStorage("LOCAL_STRING");

  // kiểm tra nếu lấy giá trị từ local mà null thì return luôn không chạy dòng phía dưới
  if (arrfromLocalStorage === null) {
    return;
  }

  arrfromLocalStorage.forEach(function (item) {
    // hàm add sẽ new obj mới và thêm (push) vào kho
    add(item);
  });

  // tạo ra obj mới NEW Employee trước khi render để có 2 function
  // 1====> <td>${item.tinhLuongTheoChucVu()}</td>
  // 2====> <td>${item.tinhTongLuong()}</td>
  // để hàm render không bị lỗi
  //render ra màn hình
  render(arrayEmployee);
}

function resetForm() {
  document.querySelector("#tknv").value = "";
  document.querySelector("#name").value = "";
  document.querySelector("#email").value = "";
  document.querySelector("#password").value = "";
  document.querySelector("#luongCB").value = "";
  document.querySelector("#chucvu").value = "Chọn chức vụ";
  document.querySelector("#gioLam").value = "";
}

function sua(itemTaiKhoanNhanVien) {
  var index = arrayEmployee.findIndex(function (item) {
    const result = item.taiKhoanNhanVien === itemTaiKhoanNhanVien;

    return result;
  });
  // console.log("này nha", index);
  // console.log("này là mảng nha ",arrayEmployee[index].taiKhoanNhanVien);

  showThongTinLenForm(arrayEmployee[index]);
  showButton('btnCapNhat');
  hideButton("btnThemNV");

}

function showThongTinLenForm(objIndex) {
  var tknvEL = document.querySelector("#tknv");
  tknvEL.value = objIndex.taiKhoanNhanVien;
  document.querySelector("#name").value = objIndex.ten;
  document.querySelector("#email").value = objIndex.email;
  document.querySelector("#password").value = objIndex.matKhau;
  document.querySelector("#datepicker").value = objIndex.ngayThang;
  document.querySelector("#luongCB").value = objIndex.luongCoBan;
  document.querySelector("#chucvu").value = objIndex.chucVu;
  document.querySelector("#gioLam").value = objIndex.gioLam;
  tknvEL.disabled = "true"; // disable không cho phép người dùng sửa số TKNV
}

document.getElementById("btnCapNhat").addEventListener("click", function (e) {
  suaThongTin();
});

function suaThongTin() {
  //lấy id
  var itemTaiKhoanNhanVien = document.getElementById("tknv").value * 1;
  var index = arrayEmployee.findIndex(function (item) {
    const result = item.taiKhoanNhanVien === itemTaiKhoanNhanVien;
    return result;
  });

  //lấy giá trị ban đầu để update tại nút "cập nhật" trong form

  // get value from html
  var objLayThongTinTuForm = layGiaTriTuForm();

  // kiểm tra dữ liệu
  // booleanObjValueCapNhat sẽ là true hoặc false được return từ validation
  var booleanObjValueCapNhat = validation(objLayThongTinTuForm);

  //Nếu value sai ==> return
  if (booleanObjValueCapNhat === false) {
    return;
  }

  //truy câợ vào kho tổng (arrayEmployee) để sửa
  update(index, objLayThongTinTuForm);

  //luu kho tổng
  luuStorage(arrayEmployee);

  // hiển thị kho tổng (arrayEmployee) ra màn hình sau khi cập nhật
  render(arrayEmployee);
}

function update(index, objLayThongTinTuForm) {
  arrayEmployee[index].ten = objLayThongTinTuForm.ten;
  arrayEmployee[index].email = objLayThongTinTuForm.email;
  arrayEmployee[index].matKhau = objLayThongTinTuForm.matKhau;
  arrayEmployee[index].ngayThang = objLayThongTinTuForm.ngayThang;
  arrayEmployee[index].luongCoBan = objLayThongTinTuForm.luongCoBan;
  arrayEmployee[index].gioLam = objLayThongTinTuForm.gioLam;
}

document.getElementById("btnTimNV").addEventListener("click", function (e) {
  searchXepLoaiNhanVien();
});

//searchName
function searchXepLoaiNhanVien() {
  var arrXepLoaiNhanVien = []; // tạo arr rỗng để lưu giá trị các phần tử sau khi search

  // lấy giá trị từ form và xóa khoảng trắng đầu cuối bằng hàm trim()
  var searchNameValue = document.getElementById("searchName").value.trim();

  // vòng lặp duyệt mảng kho tổng (arrayEmployee)
  //lặp qua từng item (object).key (xepLoai(): là function
  arrayEmployee.forEach(function (item) {
    // nếu giá trị return của function của Xếp Loại "giống" user nhập vào thì push vào arr bên dưới
    if (item.xepLoai() === searchNameValue) {
      //push item đủ điều kiện vào arrXepLoaiNhanVien(rỗng) và lưu tại đây
      arrXepLoaiNhanVien.push(item);
    }
  });

  //nếu ô search rỗng hoặc dấu cách, đồng nghĩa với string rỗng ("")
  // Thì show toàn bộ dữ liệu kho tổng đang có
  if (searchNameValue === "") {
    render(arrayEmployee);
  }

  // nếu hoàn toàn hợp lệ, thì show kết quả đã search
  else {
    render(arrXepLoaiNhanVien);
  }
}
