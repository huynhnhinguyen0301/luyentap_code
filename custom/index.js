document.getElementById("btnThemNV").addEventListener("click", function (e) {
  themNguoiDung();

});

function themNguoiDung() {
  // get value from html
  var objLayThongTinTuForm = layGiaTriTuForm();

  // kiểm tra dữ liệu
  // checkvalueValidation sẽ là true hoặc false được return từ validation
  var checkvalueValidation = validation(objLayThongTinTuForm);

  //Nếu value sai ==> return
  if (checkvalueValidation === false) {
    return;
  }

  // hàm add sẽ new obj mới và thêm (push) vào kho
  add(objLayThongTinTuForm);

  console.log(arrayEmployee);
  //luu kho tổng
  luuStorage(arrayEmployee);

  // hiển thị kho tổng (arrayEmployee) ra màn hình
  render(arrayEmployee);
  //sau khi render thì chạy hàm resetForm
  resetForm();
}

//Chạy function này khi reload trang
initLocalStorage();

function hideButton(id) {
  var button = document.getElementById(id);

  button.disabled = true;
}

function showButton(id) {
  var button = document.getElementById(id);

  button.disabled = false;
}

function handleThemNhanVien() {
  hideButton('btnCapNhat')
  showButton('btnThemNV')
}
